#!/bin/bash

# About: Merges PLY files of PMVS folder into one Dense PLY
# 
#

# !!! CURRENTLY ASSUMES THIS SCRIPT IS RUN WITH THE BASE_PATH EXPORTED FROM
#     RUNECOSYNTHER.SH SCRIPT

###############################################################################
# Change into PMVS directory
###############################################################################
cd pmvs/models
rm dense.ply
###############################################################################


HEADER_LEN=13    # Line number where header ends
AFTER_HEADER=14  # Line number where body begins


if [ $(ls *.ply | head -1) ]
	then 

	# Make Dense PLY
	FIRST_PLY=$( ls *ply | sort -n | head -1 )  # Find first ply file
	cat $FIRST_PLY | head -n $HEADER_LEN > dense.ply   # Copy header into new 'dense.ply' file
	
	TOTAL_ELE=0  # Initialize variable to track total number of elements in ply files

	for file in *.ply
	do
		if test $file != "dense.ply"  # Search through all ply files except for the new 'dense.ply'
		then
			# 1. Extract total points in file
			NUM_ELE=$(grep '^element vertex*' $file | cut -d ' '  -f3)  # Find number of elements
			TOTAL_ELE=$(($TOTAL_ELE+$NUM_ELE))  # Add elements in file to total elements

			# 2. Append points in file to dense.ply
			tail -n+$AFTER_HEADER $file >> dense.ply
		fi
	done

	# Change element vertex count
	REPLACED_LINE=$(grep '^element vertex*' dense.ply)  # Find line to replace
	sed "s/.*$REPLACED_LINE.*/element\ vertex\ $TOTAL_ELE/" dense.ply > temp.ply # Replace line
	mv temp.ply dense.ply  

else
	echo 'No PLY files in PMVS folder.'
fi


###############################################################################
# Change back to images folder
###############################################################################
cd $BASE_PATH


###############################################################################