#include <stdlib.h>
#include <string.h>
#include <vector>
#include <iostream>
using std::vector;
using std::iostream;


////////////////////////////////////////////////////////////////////////////
#if !defined(SIFTGPU_STATIC) && !defined(SIFTGPU_DLL_RUNTIME) 
// SIFTGPU_STATIC comes from compiler
#define SIFTGPU_DLL_RUNTIME
// Load at runtime if the above macro defined
// comment the macro above to use static linking
#endif

///////////////////////////////////////////////////////////////////////////
//#define DEBUG_SIFTGPU  //define this to use the debug version in windows

#ifdef _WIN32
    #ifdef SIFTGPU_DLL_RUNTIME
        #define WIN32_LEAN_AND_MEAN
        #include <windows.h>
        #define FREE_MYLIB FreeLibrary
        #define GET_MYPROC GetProcAddress
    #else
        //define this to get dll import definition for win32
        #define SIFTGPU_DLL
        #ifdef _DEBUG 
            #pragma comment(lib, "../../lib/siftgpu_d.lib")
        #else
            #pragma comment(lib, "../../lib/siftgpu.lib")
        #endif
    #endif
#else
    #ifdef SIFTGPU_DLL_RUNTIME
        #include <dlfcn.h>
        #define FREE_MYLIB dlclose
        #define GET_MYPROC dlsym
    #endif
#endif

#include "../SiftGPU/SiftGPU.h"
#include <stdio.h>

int main()
{

	// read image list: list_tmp.txt
	FILE* imglist = fopen("list_tmp.txt", "r");
	if (!imglist)
	{
		std::cout << "Cannot read from list list_tmp.txt" << '\n';
		return 0;
	}
	
	int numImg = 0;
	int temp = 0;
	
	char* tempStr = (char*)malloc(sizeof(char)*50);

	while ((temp=fscanf(imglist, "%s\n", tempStr))==1)
	{
		numImg++;
	}

	std::cout << "Read " << numImg << " images" << '\n';
	free(tempStr);
	fclose(imglist);
	
	char** imageList = (char**)malloc(sizeof(char*)*numImg);
	int i;
	for(i = 0; i < numImg; i++)
	{
		imageList[i] = (char*)malloc(sizeof(char)*250);
	}
	
	imglist = fopen("list_tmp.txt", "r");
	if (!imglist)
	{
		std::cout << "Cannot read from list list_tmp.txt" << '\n';
		return 0;
	}
	
	i = 0;
	while ((temp=fscanf(imglist, "%s\n", imageList[i])) == 1)
	{
//		std::cout << "Read image: " << imageList[i] << '\n';
		i++;
	}
	
	fclose(imglist);
	
#ifdef SIFTGPU_DLL_RUNTIME
    #ifdef _WIN32
        #ifdef _DEBUG
            HMODULE  hsiftgpu = LoadLibrary("siftgpu_d.dll");
        #else
            HMODULE  hsiftgpu = LoadLibrary("siftgpu.dll");
        #endif
    #else
        void * hsiftgpu = dlopen("libsiftgpu.so", RTLD_LAZY);
    #endif

    if(hsiftgpu == NULL) return 0;

    #ifdef REMOTE_SIFTGPU
        ComboSiftGPU* (*pCreateRemoteSiftGPU) (int, char*) = NULL;
        pCreateRemoteSiftGPU = (ComboSiftGPU* (*) (int, char*)) GET_MYPROC(hsiftgpu, "CreateRemoteSiftGPU");
        ComboSiftGPU * combo = pCreateRemoteSiftGPU(REMOTE_SERVER_PORT, REMOTE_SERVER);
        SiftGPU* sift = combo;
        SiftMatchGPU* matcher = combo;
    #else
        SiftGPU* (*pCreateNewSiftGPU)(int) = NULL;
        SiftMatchGPU* (*pCreateNewSiftMatchGPU)(int) = NULL;
        pCreateNewSiftGPU = (SiftGPU* (*) (int)) GET_MYPROC(hsiftgpu, "CreateNewSiftGPU");
        pCreateNewSiftMatchGPU = (SiftMatchGPU* (*)(int)) GET_MYPROC(hsiftgpu, "CreateNewSiftMatchGPU");
        SiftGPU* sift = pCreateNewSiftGPU(1);
        SiftMatchGPU* matcher = pCreateNewSiftMatchGPU(4096);
    #endif

#elif defined(REMOTE_SIFTGPU)
    ComboSiftGPU * combo = CreateRemoteSiftGPU(REMOTE_SERVER_PORT, REMOTE_SERVER);
    SiftGPU* sift = combo;
    SiftMatchGPU* matcher = combo;
#else
    //this will use overloaded new operators
    SiftGPU  *sift = new SiftGPU;
    SiftMatchGPU *matcher = new SiftMatchGPU(4096);
#endif
    vector<float > descriptors1(1), descriptors2(1);
    vector<SiftGPU::SiftKeypoint> keys1(1), keys2(1);    
    int num1 = 0, num2 = 0;    

    char * argv[] = {"-fo", "-1",  "-v", "1"};

    int argc = sizeof(argv)/sizeof(char*);
    sift->ParseParam(argc, argv);

	FILE* fmatches = fopen("matches.init.txt", "w");
	if (!fmatches)
	{
		std::cout << "Cannot open file matches.init.txt" << '\n';
		return 0;
	}

    //Create a context for computation, and SiftGPU will be initialized automatically 
    //The same context can be used by SiftMatchGPU
    if(sift->CreateContextGL() != SiftGPU::SIFTGPU_FULL_SUPPORTED) return 0;

	int j=0;
	
	for (i = 0; i < numImg; i++)
	{
		if(sift->RunSIFT(imageList[i]))
    	{
    	
    		std::string filename = imageList[i];
    		int stringlen = filename.length();
    		filename[stringlen-1] = 'y';
    		filename[stringlen-2] = 'e';
    		filename[stringlen-3] = 'k';
    		
    		sift->SaveSIFT((char*)filename.c_str());
//		    	std::cout << "Image " << imageList[i] << '\n';
    		num1 = sift->GetFeatureNum();
    		keys1.resize(num1);    descriptors1.resize(128*num1);
        	sift->GetFeatureVector(&keys1[0], &descriptors1[0]);        	
    	}
		for (j = 0; j < i; j++)
		{
		    if(sift->RunSIFT(imageList[j]))
    		{
//		    	std::cout << "Image " << imageList[j] << '\n';
        		num2 = sift->GetFeatureNum();
        		keys2.resize(num2);    descriptors2.resize(128*num2);
		        sift->GetFeatureVector(&keys2[0], &descriptors2[0]);
    		}

		    matcher->VerifyContextGL(); //must call once

		    matcher->SetDescriptors(0, num1, &descriptors1[0]); //image 1
		    matcher->SetDescriptors(1, num2, &descriptors2[0]); //image 2

		    int (*match_buf)[2] = new int[num1][2];
		    int num_match = matcher->GetSiftMatch(num1, match_buf);
    
    		if (num_match >= 8)
    		{
		    	fprintf(fmatches, "%d %d\n", j, i);
		    	fprintf(fmatches, "%d\n", num_match);
    
			    for(int i  = 0; i < num_match; ++i)
			    {
			        fprintf(fmatches, "%d %d\n", match_buf[i][1], match_buf[i][0]);
	    		}
			}
			    		
   		    delete[] match_buf;
		}		
	}
	
/*
    if(sift->RunSIFT(imageList[0]))
    {
    	std::cout << "Image " << imageList[0] << '\n';
        num1 = sift->GetFeatureNum();
        keys1.resize(num1);    descriptors1.resize(128*num1);
        sift->GetFeatureVector(&keys1[0], &descriptors1[0]);
    }

    if(sift->RunSIFT(imageList[1]))
    {
    	std::cout << "Image " << imageList[1] << '\n';
        num2 = sift->GetFeatureNum();
        keys2.resize(num2);    descriptors2.resize(128*num2);
        sift->GetFeatureVector(&keys2[0], &descriptors2[0]);
    }

    matcher->VerifyContextGL(); //must call once

    matcher->SetDescriptors(0, num1, &descriptors1[0]); //image 1
    matcher->SetDescriptors(1, num2, &descriptors2[0]); //image 2

    int (*match_buf)[2] = new int[num1][2];
    int num_match = matcher->GetSiftMatch(num1, match_buf);
    
    fprintf(fmatches, "%d %d\n", i, j);
    
    for(int i  = 0; i < num_match; ++i)
    {
        fprintf(fmatches, "%d %d\n", match_buf[i][0], match_buf[i][1]);
    }
*/

	for(i = 0; i < numImg; i++)
	{
		free(imageList[i]);
	}
	free(imageList);
	
	fclose(fmatches);
	
    // clean up..

#ifdef REMOTE_SIFTGPU
    delete combo;
#else
    delete sift;
    delete matcher;
#endif

    return 1;
}

