# Ecosynther at a Glance

**Ecosynth** is a lab at UMBC developing a suite of tools used to map and measure vegetation in three dimensions using off-the-shelf digital cameras and open-source computer vision (CV) software, from the ground or using low altitude  (<130m) light-weight aircraft.

**Ecosynther** is an image processing program that generates 3D scans of vegetation from sets of digital photographs acquired from these missions on ground and at low altitude from light-weight aircraft.

## The Ecosynth Pipeline

![Ecosynth Pipeline](https://doc-00-40-docs.googleusercontent.com/docs/securesc/nv3gj3qlo6114dhuo3r2dpb01tivdpe6/in8qo7uq6jp8q52be4fbhhccq1q5etf2/1426773600000/03179130569403700848/03179130569403700848/0B2v3HbtEo7-7Sm42Z3lzQ1hUaDA?nonce=9dbua5v6d96v6&user=03179130569403700848&hash=7mdbl1ci986foqsidbgukfaso3nfga6i)

## The Ecosynth Data-Structure

The following data ‘objects’ are collections of data generated after each stage in the pipeline is completed.  The Ecosynther program uses data from ‘Acquisition Objects’ as input and generates ‘Processed-Data Objects’ as outputs.  Data from the ‘Processed-Data Objects can be used to generate noise-filtered and georeferenced ‘Point Cloud Objects’ further down the pipeline with the EcosynthAerial toolchain.

### Acquisition Object

    - Mission Information
        * Site Name
        * Date & Time
        * Weather Conditions
        * Area of Interest Lat & Long Boundaries	
        * Method of Data Collection (e.g. UAV or Ground vehicle used)
    - Photos
    - Route
        * LOG File
        * GPS Positions (Filtered from LOG File, and currently must be formatted using the UTM coordinate system)

### Processed-Data Object

    - Post-Processing Information
        * CV Software Type and Version
        * Run Date
        * Testing Summary
            - Timing Breakdown
            - Highest Test-Level Passed
                * Photos Used (%, Absolute)
    - Point Cloud Object (Sparse)
    - Point Cloud Object (Dense)
    - OUT File
    - 3D Visualization Object

### Point Cloud Object

    - Point Cloud Information
        * Point Cloud Type
        * Mean Point Cloud Density
        * Georeferencing Path Fit
    - PLY File (Filtered and Georeferenced)