/* 
 *  Copyright (c) 2008-2010  Noah Snavely (snavely (at) cs.cornell.edu)
 *    and the University of Washington
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */

/* KeyMatchFull.cpp */
/* Read in keys, match, write results to a file */

#include <assert.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

#include "keys2a.h"

//move around as needed
const int MAX_MATCHES = 500;

int main(int argc, char **argv) {
    char *list_in;
    char *file_out;
    //this is the list of photos that are close
    char *matches_in;
    //
    double ratio;
    
    if (argc != 3) {
	printf("Usage: %s <list.txt> <outfile>\n", argv[0]);
	return -1;
    }
    
    list_in = argv[1];
    ratio = 0.6;
    file_out = argv[2];        

    clock_t start = clock();

    unsigned char **keys;
    int *num_keys;

    /* Read the list of files */
    //if I want to sort these do it here, then go back into my python and sort there too
    //sort by name
    std::vector<std::string> key_files;
    
    FILE *f = fopen(list_in, "r");
    if (f == NULL) {
        printf("Error opening file %s for reading\n", list_in);
        return 1;
    }

    char buf[512];
    while (fgets(buf, 512, f)) {
        /* Remove trailing newline */
        if (buf[strlen(buf) - 1] == '\n')
            buf[strlen(buf) - 1] = 0;
        
        key_files.push_back(std::string(buf));
    }

    fclose(f);

    f = fopen(file_out, "w");
    assert(f != NULL);

    int num_images = (int) key_files.size();

    //Ecosytnther stuff
    //why won't it let mee add this? whatever
    //    matches_in = argv[3]; 
    matches_in = "pointMatches.txt";
    std::ifstream matchesFile;
    matchesFile.open(matches_in);

    //array of images and their matches
    int matches[num_images][MAX_MATCHES];

    if (!matchesFile) {
      std::cout << "Failed to read in matches file\n";
      return 0;
    }

    //read in the indexes from the file
    char *line = (char*) malloc(sizeof(char) * 8001);
    int imageIndex = 0;
    char *place;
    char *oldPlace;
        
    while (matchesFile.getline(line, 8000)){
      // std::cout << "test2\n";	
      int lineIndex = 0;
      place = line;
      
      while(place != NULL) {
	oldPlace = place;
	place = strchr(oldPlace + 1, ' ');
	if (place == NULL){
	  break;
	}
	int i = 0;
	char num[10];
	
	for(i = 0; oldPlace[i] != place[0]; i++) {
	    num[i] = oldPlace[i];
	}

	num[i] = '\0';
	matches[imageIndex][lineIndex] = atoi(num);
	lineIndex++;
      }

      //indicates the end of the list of relevant numbers
      matches[imageIndex][lineIndex] = -1;
      imageIndex++;
    }
    
    //matches should now have the info taht the file had in it now
    matchesFile.close();
    free(line);
    //End Ecosynther stuff


    
    keys = new unsigned char *[num_images];
    num_keys = new int[num_images];

    /* Read all keys */
    for (int i = 0; i < num_images; i++) {
        keys[i] = NULL;
        num_keys[i] = ReadKeyFile(key_files[i].c_str(), keys+i);
    }

    clock_t end = clock();    
    printf("[KeyMatchFull] Reading keys took %0.3fs\n", 
           (end - start) / ((double) CLOCKS_PER_SEC));
    
    for (int i = 0; i < num_images; i++) {
        if (num_keys[i] == 0)
            continue;

        printf("[KeyMatchFull] Matching to image %d\n", i);

        start = clock();

        /* Create a tree from the keys */
        ANNkd_tree *tree = CreateSearchTree(num_keys[i], keys[i]);
	int j = 0;

	//Make change here to compare close images once you have the thing
	//matches is the list
	//for (index in matches[i])
	//changed this for loop to only look at image indicies (indexes?) we want it to

        for (int k = 0; matches[i][k] != -1 ; k++) {
	  j = matches[i][k];

	  //Could the problem be that indexing thing I was scared of? maybe?
	  //how to fix:
	  //sort all the things
	  //figure out where they read in their stuff and try to replicate it

            if (num_keys[j] == 0)
                continue;

            /* Compute likely matches between two sets of keypoints */
            std::vector<KeypointMatch> matches = 
                MatchKeys(num_keys[j], keys[j], tree, ratio);
            
            int num_matches = (int) matches.size();

            if (num_matches >= 16) {
                /* Write the pair */
                fprintf(f, "%d %d\n", j, i);

                /* Write the number of matches */
                fprintf(f, "%d\n", (int) matches.size());

                for (int i = 0; i < num_matches; i++) {
                    fprintf(f, "%d %d\n", 
                            matches[i].m_idx1, matches[i].m_idx2);
                }
            }
        }

        end = clock();    
        printf("[KeyMatchFull] Matching took %0.3fs\n", 
               (end - start) / ((double) CLOCKS_PER_SEC));
        fflush(stdout);

        // annDeallocPts(tree->pts);
        delete tree;
    }
    
    /* Free keypoints */
    for (int i = 0; i < num_images; i++) {
        if (keys[i] != NULL)
            delete [] keys[i];
    }
    delete [] keys;
    delete [] num_keys;
    
    fclose(f);
    return 0;
}
