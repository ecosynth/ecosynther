# cross track matching assessment 

#v1: created, import make sift_tiles_v6.py to use those functions
'''
- currently configured for only the SERC a_2011_10_08_m2 test datset fof 596 cameras, arranged as below
- Cross track matching is identified for the left two sets Set1 and the right two sets Set2
- Set 2 contains the major observable error
- Set 1 appears OK
- goal is to create a point cloud of cross tracking matching points and a raster map
 


- goal is to create a point cloud of cross tracking matching points and a raster map
 1A			1B----------2A			2B
 1A			1B			2A			2B
 1A			1B			2A			2B
 1A			1B			2A			2B
 1A			1B			2A			2B
 1A			1B			2A			2B
 1A			1B			2A			2B
 1A			1B			2A			2B
 1A			1B			2A			2B
 1A			1B			2A			2B
 1A			1B			2A			2B
 1A---------1B			2A----------2B

'''

import make_sift_tiles_v6 as TILES
import numpy as NP
import matplotlib as PLT
from Tkinter import Tk
from tkFileDialog import askopenfilename, askdirectory
import time

#start a timer
startTime = time.time()

###########################################################################
def SimpleViewList(viewList):
	print '\nSimplifying View List'
	plainViewList = []
	for row in viewList:
		splitViewLine = row.rstrip().split(' ')
		viewArray = NP.array(splitViewLine[1:], dtype =object).reshape(int(splitViewLine[0]),4)
		viewCams = list(NP.array(viewArray[:,0], dtype=int))
		plainViewList.append(viewCams)

	return NP.array(plainViewList, dtype=object)

###########################################################################
def main():

	# set up the image sets - counting from 1 as in Bundler list.txt
	Set1A = NP.arange(459, 597)
	Set1B = NP.arange(309, 440)
	Set1 = Set1A, Set1B

	Set2A = NP.arange(154, 289)
	Set2B = NP.arange(1, 130)
	Set2 = Set2A, Set2B

	Set3 = Set1B, Set2A

	AllSets = Set1A, Set1B, Set2A, Set2B

	
	#get the bundle.out file
	Tk().withdraw()
	print '\nPlease select the bundle.out file to be analyzed'
	BundleOut = askopenfilename(title='Please select the Bundle.out file')

	#get Params file
	print '\nPlease select the 7 parameters file for georeferencing'
	ParamsFile = askopenfilename(title='Please select the params file')

	print BundleOut#, ParamsFile

	#get output directory location
	print '\nPlease select the output directory'
	outDirectory = askdirectory(title='Please select the output directory')
	print outDirectory
	
	# temp file / directory variables
	#outDirectory = '/media/Ecosynth_PICTURES/Ecosynth_feature_ID_working/a_se_2011_10_08_m2/pics/test_set_Ecosynther/features'
	#BundleOut = '/media/Ecosynth_PICTURES/Ecosynth_feature_ID_working/a_se_2011_10_08_m2/pics/test_set_Ecosynther/bundle/bundle_596.out'
	#ParamsFile = '/media/Ecosynth_PICTURES/Ecosynth_feature_ID_working/a_se_2011_10_08_m2/pics/test_set_Ecosynther/features/stats/params_from_spline.txt'
	params = NP.loadtxt(ParamsFile)


    #specify .out list
	print '\nReading in bundle.out'
	outList = open(BundleOut, 'r')
	outListRead = outList.readlines()
	outList.close()

	# build an array of Index, georeferenecd XYZ, RGB, and the view list object
	newViewList, XYZRGBarray, cameras, numberPoints = TILES.PointList(outListRead)
	XYZRGBarray[:,0:3] = TILES.ApplyHelmert(params, XYZRGBarray[:,0:3])
	pointIDX = NP.arange(XYZRGBarray.shape[0], dtype=int)
	XYZRGBarray = NP.vstack((pointIDX,XYZRGBarray.transpose())).transpose()
	plainViewList = SimpleViewList(newViewList)
	XYZRGBViews = NP.vstack((XYZRGBarray.transpose(),plainViewList)).transpose()

	# for each point row, check to see which set its in and if it is a cross track
	print '\nChecking for cross track matches'
	Set1Matches = NP.empty_like(XYZRGBViews)
	Set2Matches = NP.empty_like(XYZRGBViews)
	Set3Matches = NP.empty_like(XYZRGBViews)
	numberPoints = XYZRGBarray.shape[0]
	pointCounter = 0
	Set1Counter = 0
	Set2Counter = 0
	Set3Counter = 0
	for row in XYZRGBViews:
		#check Set1 
		Check1A = NP.in1d(row[-1], Set1A)
		Check1B = NP.in1d(row[-1], Set1B)
		if NP.any(Check1A) == True &  NP.any(Check1B) == True:
			Set1Matches[Set1Counter] = row
			Set1Counter += 1

		#check Set2
		Check2A = NP.in1d(row[-1], Set2A)
		Check2B = NP.in1d(row[-1], Set2B)
		if NP.any(Check2A) == True & NP.any(Check2B) == True:
			#print row
			Set2Matches[Set2Counter] = row
			Set2Counter += 1

		#check Set3
		#Set3 = Set1B, Set2A
		if NP.any(Check2A) == True & NP.any(Check1B) == True:
			#print row
			Set3Matches[Set3Counter] = row
			Set3Counter += 1

		pointCounter += 1
		if pointCounter%10000 == 0:        
			print '\n%.0f %% (%u of %u) points processed in %.2f minutes'%((float(pointCounter)/float(numberPoints))*100., pointCounter, numberPoints, ((time.time() - startTime) /60.) ) 
			print '%u Set 1 Cross track matches'%(Set1Counter)
			print '%u Set 2 Cross track matches'%(Set2Counter)
			print '%u Set 3 Cross track matches'%(Set3Counter)

	Set1Matches = Set1Matches[:Set1Counter]
	Set2Matches = Set2Matches[:Set2Counter]
	Set3Matches = Set3Matches[:Set3Counter]
	
	#save the sets
	NP.savetxt('%s/Set1Matches_size_%u.txt'%(outDirectory, Set1Counter), Set1Matches, delimiter=',', fmt='%u, %.3f, %.3f, %.3f, %u, %u, %u, %s')
	NP.savetxt('%s/Set2Matches_size_%u.txt'%(outDirectory, Set2Counter), Set2Matches, delimiter=',', fmt='%u, %.3f, %.3f, %.3f, %u, %u, %u, %s')
	NP.savetxt('%s/Set3Matches_size_%u.txt'%(outDirectory, Set3Counter), Set3Matches, delimiter=',', fmt='%u, %.3f, %.3f, %.3f, %u, %u, %u, %s')
	
	# add a header
	newHeader = 'IDX, X, Y, Z, R, G, B, ViewList'
	
	Set1file = open('%s/Set1Matches_size_%u.txt'%(outDirectory, Set1Counter), 'r')
	Set1out = open('%s/Set1Matches_size_%u_header.txt'%(outDirectory, Set1Counter), 'w')
	Set1out.write(newHeader + '\n')
	Set1fileRead = Set1file.readlines()
	for line in Set1fileRead:
		Set1out.write(line)
	Set1file.close()
	Set1out.close()	

	Set2file = open('%s/Set2Matches_size_%u.txt'%(outDirectory, Set2Counter), 'r')
	Set2out = open('%s/Set2Matches_size_%u_header.txt'%(outDirectory, Set2Counter), 'w')
	Set2out.write(newHeader + '\n')
	Set2fileRead = Set2file.readlines()
	for line in Set2fileRead:
		Set2out.write(line)
	Set2file.close()
	Set2out.close()	

	Set3file = open('%s/Set3Matches_size_%u.txt'%(outDirectory, Set3Counter), 'r')
	Set3out = open('%s/Set3Matches_size_%u_header.txt'%(outDirectory, Set3Counter), 'w')
	Set3out.write(newHeader + '\n')
	Set3fileRead = Set3file.readlines()
	for line in Set3fileRead:
		Set3out.write(line)
	Set3file.close()
	Set3out.close()

	

		
'''
>>> test = np.array([0, 1, 2, 5, 0])
>>> states = [0, 2]
>>> mask = np.in1d(test, states)
>>> mask
array([ True, False,  True, False,  True], dtype=bool)
>>> test[mask]
array([0, 2, 0])
'''


###########################################################################

if __name__ == "__main__":
	main()