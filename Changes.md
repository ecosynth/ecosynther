Ecosynther Change Log
=====================

Aug 24, 2013
	* Fixed messy file output problem
	* Added densification process
	

Jul 2, 2013
	* Added instruction to compile and the run the application
	* Created repository for Ecosynther
