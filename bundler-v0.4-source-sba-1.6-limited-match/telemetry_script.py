"""
Functions for Telemetry go here
"""

import os, sys, re, StringIO


#### MikroKopter Functions

def OpenText(fileName): #Open up input telem file and read to line file
    infile = open(fileName, "r")
    inlines = infile.readlines()
    infile.close()
    return inlines
	
def SingleVal(i, name): # a function for getting a value from a line, saves a few lines, i is the line, name is the word to cut
    bname = "<" + name + ">"
    ename = "</" + name + ">\r\n"
    value = i.lstrip(bname).rstrip(ename)
    return value + ","

def writeCSV_MK(lines, outputName):
	outfile = open(outputName,"w") #will be in the current working directory
	outfile.write("lat,lon,elev,date,zulutime,satellites,blank,altimeter,variometer,course,groundspeed,")
	outfile.write("vertspeed,flighttime,voltage,current,capacity,RCquality,RCRSSI,compass,")
	outfile.write("nickangle,rollangle,NCflag,Errorcode,targetbearing,targetdistance,")
	outfile.write("RCsticks1,RCsticks2,RCsticks3,GPSsticks1,GPSsticks2,GPSsticks3" + "\n")
	for i in lines[12:-5]:
		if i.startswith("<trkpt lat") == True:
			lat,lon = i.split(" lon=")
			lat = lat.split('lat="+')[-1].strip('"')
			lon = lon.strip(">\r\n").strip('"')
			outfile.write(lat + "," + lon + ",")
           
		elif i.startswith("<ele>") == True: #read ele
			outfile.write(SingleVal(i, "ele"))
            
		elif i.startswith("<time>") == True: # read time,date
			date,time = i.split("T")
			date = date.lstrip("<time>")
			time = time.rstrip("Z</time>\r\n")
			outfile.write(date + "," + time + ",")
            
		elif i.startswith("<sat>") == True:
			outfile.write(SingleVal(i, "sat"))
            
		elif i.startswith("<extensions>") == True: # I couldn't figure out how to get it skip this row
			outfile.write("NULL" + ",")
            
		elif i.startswith("<Altimeter>") == True:
			outfile.write(SingleVal(i,"Altimeter"))
            
		elif i.startswith("<Variometer>") ==  True:
			outfile.write(SingleVal(i,"Variometer"))
            
		elif i.startswith("<Course>") == True:
			outfile.write(SingleVal(i,"Course"))
            
		elif i.startswith("<GroundSpeed>") == True:
			outfile.write(SingleVal(i, "GroundSpeed"))
            
		elif i.startswith("<VerticalSpeed>") == True:
			outfile.write(SingleVal(i,"VerticalSpeed"))
        
		elif i.startswith("<FlightTime>") == True:
			outfile.write(SingleVal(i, "FlightTime"))
        
		elif i.startswith("<Voltage>") == True:
			outfile.write(SingleVal(i,"Voltage"))
            
		elif i.startswith("<Current>") == True:
			outfile.write(SingleVal(i, "Current"))
        
		elif i.startswith("<Capacity>") == True:
			outfile.write(SingleVal(i, "Capacity"))
            
		elif i.startswith("<RCQuality>") == True:
			outfile.write(SingleVal(i, "RCQuality"))
            
		elif i.startswith("<RCRSSI>") == True:
			outfile.write(SingleVal(i, "RCRSSI"))
        
		elif i.startswith("<Compass>") == True:
			outfile.write(SingleVal(i, "Compass"))
            
		elif i.startswith("<NickAngle>") == True:
			outfile.write(SingleVal(i, "NickAngle"))
        
		elif i.startswith("<RollAngle>") == True:
			outfile.write(SingleVal(i, "RollAngle"))
            
		elif i.startswith("<NCFlag>") == True:
			outfile.write(SingleVal(i, "NCFlag"))
            
		elif i.startswith("<ErrorCode>") == True:
			outfile.write(SingleVal(i, "ErrorCode"))
            
		elif i.startswith("<TargetBearing>") == True:
			outfile.write(SingleVal(i, "TargetBearing"))
            
		elif i.startswith("<TargetDistance>") == True:
			outfile.write(SingleVal(i, "TargetDistance"))
            
		elif i.startswith("<RCSticks>") == True:
			RCSticks1, RCSticks2, RCSticks3 = i.split(",")
			RCSticks1 = RCSticks1.lstrip("<RCSticks>")
			RCSticks2 = RCSticks2.lstrip(" ")
			RCSticks3 = RCSticks3.rstrip("</RCSticks>\r\n").strip(" ")
			outfile.write(RCSticks1 + "," + RCSticks2 + "," + RCSticks3 + ",")
        
		elif i.startswith("<GPSSticks>") == True:
			GPSSticks1, GPSSticks2, GPSSticks3 = i.split(",")
			GPSSticks1 = GPSSticks1.lstrip("<GPSSticks>")
			GPSSticks2 = GPSSticks2.lstrip(" ")
			GPSSticks3 = GPSSticks3.rstrip("</GPSSticks>\r\n").strip(" ")
			outfile.write(GPSSticks1 + "," + GPSSticks2 + "," + GPSSticks3)
			
		elif i.startswith("</trkpt>") == True:
			outfile.write("\n")
			



##### ARDU Functions

def writeCSV_AR(commandList, fileName):
    outfile = open(fileName, 'w')
    outfile.write("Latitude, Longitude, Elevation, Time, Date, Course, Roll, Pitch\n")
    for item in commandList:
        outfile.write(item.latitude + ', ' + 
                   item.longitude + ', ' + 
                   item.elevation + ', ' + 
                   item.time + ', ' + 
                   item.date + ', ' + 
                   item.course + ', ' + 
                   item.roll + ', ' + 
                   item.pitch + '\n')


def makeCmdList(insString):
    #the format I am dealing with looks like this:
    #<trkpt lat="number" lon="number">
    #<ele>number</ele>
    #<time>time string</time>
    #<course>number</course>
    #<roll>number</roll>
    #<pitch>number</pitch>
    #</trkpt>

    #chop it up the the </trkpt> tag
    entryList = re.split('</trkpt>', insString)
    trkptList = []

    #gets rid of the last element, which is a junk string
    entryList.pop()

    for entry in entryList:
        currentTrkpt = trkpt()
        
        #regular expressions are our friends
        result = re.search('<trkpt lat="(?P<latitude>\S*)" lon="(?P<longitude>\S*)"><ele>(?P<elevation>\S*)</ele><time>(?P<date>\S*)T(?P<time>\S*)</time><course>(?P<course>\S*)</course><roll>(?P<roll>\S*)</roll><pitch>(?P<pitch>\S*)</pitch>', entry)

        #feed the data into currentTrkpt
        currentTrkpt.latitude = result.group("latitude")
        currentTrkpt.longitude = result.group("longitude")
        currentTrkpt.elevation = result.group("elevation")
        currentTrkpt.time = result.group("time")
        currentTrkpt.date = result.group("date")
        currentTrkpt.course = result.group("course")
        currentTrkpt.roll = result.group("roll")
        currentTrkpt.pitch = result.group("pitch")

        #put it onto the end of the list
        trkptList.append(currentTrkpt)
    
    #spit the list out
    return trkptList

#data structure to store the different track points
class trkpt:
    #store as strings
    latitude = ""
    longitude = ""
    elevation = ""
    time = ""
    date = ""
    course = ""
    roll = ""
    pitch = ""

    def __init__(self):
        self.latitude = ""
        self.longitude = ""
        self.elevation = ""
        self.time = ""
        self.date = ""
        self.course = ""
        self.roll = ""
        self.pitch = ""


def telemetry_main(file_format, filename, file_string):
    if (file_format == "ardu"):
	outputName = filename + '_convertedAR.txt'
        list = makeCmdList(file_string)
        writeCSV_AR(list, outputName)

    else:
	outputName = filename + '_convertedMK.txt'
	buffer = StringIO.StringIO(file_string)
	file_lines = buffer.readlines()
	buffer.close()
        writeCSV_MK(file_lines, outputName)
    
    return

telemetry_main("ardu", "current", OpenText(sys.argv[1])[0])
