#include <assert.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

const int MAX_MATCHES = 500;

int main(int argc, char **argv) {

    char *matches_in;
    int num_images = 1561;
    //Ecosytnther stuff

    matches_in = argv[1]; 
    std::ifstream matchesFile;
    matchesFile.open(matches_in);

    //array of images and their matches
    int matches[num_images][MAX_MATCHES];

    if (!matchesFile) {
      std::cout << "Failed to read in matches file\n";
      return 0;
    }

    //read in the indexes from the file
    char *line = (char*) malloc(sizeof(char) * 7001);
    int imageIndex = 0;
    char *place;
    char *oldPlace;
        
    while (matchesFile.getline(line, 7000)){
      // std::cout << "test2\n";	
      int lineIndex = 0;
      place = line;
      
      while(place != NULL) {
	oldPlace = place;
	place = strchr(oldPlace + 1, ' ');
	if (place == NULL){
	  break;
	}
	int i = 0;
	char num[10];
	
	for(i = 0; oldPlace[i] != place[0]; i++) {
	    num[i] = oldPlace[i];
	}

	num[i] = '\0';
	matches[imageIndex][lineIndex] = atoi(num);
	lineIndex++;
      }

      //indicates the end of the list of relevant numbers
      matches[imageIndex][lineIndex] = -1;
      imageIndex++;
    }
    

    //matches should now have the info taht the file had in it now
    matchesFile.close();
    free(line);
    //End Ecosynther stuff
    
    for (int i = 0; i < 50; i++) {
      std::cout << matches[i][0];
      std::cout << '\n';
    }
    return 0;
}
