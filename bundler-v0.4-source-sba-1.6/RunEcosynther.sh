#!/bin/bash
#
# RunBundler.sh
#   copyright 2008 Noah Snavely
#
# A script for preparing a set of image for use with the Bundler 
# structure-from-motion system.
#
# Usage: RunBundler.sh [image_dir]
#
# The image_dir argument is the directory containing the input images.
# If image_dir is omitted, the current directory is used.
#

# Set this variable to your base install path (e.g., /home/foo/bundler)
# BASE_PATH="TODO"
#BASE_PATH=$(dirname $(which $0));

BASE_PATH="/home/tseneschal/Desktop/ecosynther/bundler-v0.4-source-sba-1.6"

timestamp=`date`
echo "$timestamp Starting RunBundler" > timelog

if [ $BASE_PATH == "TODO" ]
then
    echo "Please modify this script (RunBundler.sh) with the base path of your bundler installation.";
    exit;
fi

timestamp=`date`
echo "$timestamp Starting ExtractFocal" >> timelog

EXTRACT_FOCAL=$BASE_PATH/bin/extract_focal.pl

OS=`uname -o`

if [ $OS == "Cygwin" ]
then
    MATCHKEYS=$BASE_PATH/bin/KeyMatchFull.exe
    BUNDLER=$BASE_PATH/bin/Bundler.exe
else
    MATCHKEYS=$BASE_PATH/bin/KeyMatchFull
    BUNDLER=$BASE_PATH/bin/bundler
fi

timestamp=`date`
echo "$timestamp Starting SIFT" >> timelog

TO_SIFT=$BASE_PATH/bin/ToSift.sh

IMAGE_DIR="."

if [ $# -eq 1 ]
then
    echo "Using directory '$1'"
    IMAGE_DIR=$1
fi

# Rename ".JPG" to ".jpg"
for d in `ls -1 $IMAGE_DIR | egrep ".JPG$"`
do 
    mv $IMAGE_DIR/$d $IMAGE_DIR/`echo $d | sed 's/\.JPG/\.jpg/'`
done

# Create the list of images
find $IMAGE_DIR -maxdepth 1 | egrep ".jpg$" | sort > list_tmp.txt
$EXTRACT_FOCAL list_tmp.txt
cp prepare/list.txt .

cp list_tmp.txt ../CustomizeGPUSIFT/SiftGPU/bin/
cd ../CustomizeGPUSIFT/SiftGPU/bin/
./SimpleSIFT

EXTENSION=".gz"

timestamp=`date`
echo "$timestamp Starting Keypoint Matching" >> timelog

for d in `ls -1 $IMAGE_DIR | egrep "jpg$"`
do
	key_file=$IMAGE_DIR`echo $d | sed 's/jpg$/key/'`
	echo "gzip -f $key_file"
	gzip -f $key_file
	cp $key_file$EXTENSION ../../../bundler-v0.4-source-sba-1.6/$IMAGE_DIR
done	

cp matches.init.txt ../../../bundler-v0.4-source-sba-1.6/
cd ../../../bundler-v0.4-source-sba-1.6/

# Generate the options file for running bundler 
mkdir bundle
rm -f options.txt

echo "--match_table matches.init.txt" >> options.txt
echo "--output bundle.out" >> options.txt
echo "--output_all bundle_" >> options.txt
echo "--output_dir bundle" >> options.txt
echo "--variable_focal_length" >> options.txt
echo "--use_focal_estimate" >> options.txt
echo "--constrain_focal" >> options.txt
echo "--constrain_focal_weight 0.0001" >> options.txt
echo "--estimate_distortion" >> options.txt
echo "--run_bundle" >> options.txt

timestamp=`date`
echo "$timestamp Starting Bundler" >> timelog

# Run Bundler!
echo "[- Running Bundler -]"
rm -f constraints.txt
rm -f pairwise_scores.txt
#valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --num-callers=20 --track-fds=yes $BUNDLER list.txt --options_file options.txt > bundle/out
$BUNDLER list.txt --options_file options.txt > bundle/out

echo "[- Done -]"
timestamp=`date`
echo "$timestamp Finished" >> timelog