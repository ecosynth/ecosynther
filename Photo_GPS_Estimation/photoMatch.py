import sys
import utm
import scipy
import math
import scipy.spatial as sp
import numpy as np

#take the files and run from commandline
#the input is the outfile from GPS_camera_telem_interp.py
def main(picGPS, radius = 50):
    points = getPhotoPositions(picGPS)
    pointMatches = matchPoints(points, radius)
    printFile(pointMatches)
    return

#builds associations bewteen the GPS positions and the photos
#not entirely automated, gets a list of pics with associated XYZ
def getPhotoPositions(photoData):
    #sort the images Kin the same order as bundler
    picData = open(photoData, 'r').read()
    picData = picData.split('\n')

    # print "before pop:"
    # for line in picData:
    #     print line

    #gets rid of the header and the junk newline at the end
    picData.pop()
    picData.pop(0)

    #sort the images by name
    picData.sort()

    print "after pop:"
    for line in picData:
        print line

    for i in range(len(picData)):
        picData[i] = picData[i].split('\t')

    #discard the photo names for now
    picData = np.array(picData)
    picLocal = np.array(picData[:, 1:3], dtype=float)
    print picLocal
    return picLocal

#Matches the points using the KDtree from scipy
def matchPoints(pointList, radius):
    pointMatches = []

    #Keep this around in case the numpy system doesn't work for whatever rason
    #Brute Force
    # for i in range(len(pointList)):
    #     pointMatches.append([])
    #     for j in range(len(pointList)):
    #         dis = distance(pointList[i], pointList[j])
    #         if (i != j and dis <= MAX_DISTANCE):
    #             pointMatches[i].append(j)

    #UTM = getUTM(pointList) #not needed if the GPS points are already moved over to UTM
    pointMatches = UTMtree(pointList, radius)

    pointMatches = np.array(pointMatches)

    return pointMatches

#prints out the file
def printFile(matches):
    outFile = open("pointMatches.txt", 'w')
    for line in matches:
        for i in range(len(line)):
            outFile.write(str(line[i]))
            outFile.write(' ')

        outFile.write('\n')
    
    return

#not used in current version, kept for the broute force backup
def distance(point1, point2):
    radius = 6371000 #meters
    dLat = math.radians(point1[0] - point2[0]) #(lat2-lat1).toRad()
    dLon = math.radians(point1[1] - point2[1]) #(lon2-lon1).toRad()
    lat1 = point1[0] #lat1.toRad()
    lat2 = point2[0] #lat2.toRad()

    a = (math.sin(dLat/2) * math.sin(dLat/2)) + (math.sin(dLon/2) * math.sin(dLon/2)) * math.cos(lat1) * math.cos(lat2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c

    return d

def getUTM(GPSpoints):
    UTMpoints = []

    for i in range(len(GPSpoints)):
       point = utm.from_latlon(GPSpoints[i][0], GPSpoints[i][1])
       UTMpoints.append([point[0], point[1]])

    return UTMpoints

def UTMtree(UTMpoints, radius):
    
    #O(nlogn)
    pointTree = sp.KDTree(UTMpoints)

    #O(nlogn)
    pointMatches = pointTree.query_ball_tree(pointTree, radius)

    delList = []
    
    #Stops the images from comparing to themselves
    for i in range(len(pointMatches)):
        delList.append([])
        for j in range(len(pointMatches[i])):
            if i == pointMatches[i][j]:
                delList[i].append(j)                

    #Deletes images being compaired to themselves
    for i in range(len(pointMatches)):
        pointMatches[i] = np.delete(pointMatches[i], delList[i])

    return pointMatches

main(sys.argv[1], sys.argv[2])
