# this script interpolates the GPS position from a GPS list 
# onto the camera list

import os, sys
from numpy import *


def scaleList(GPS_array, CAM_array):
    GPSlength = float(GPS_array.shape[0])
    CAMlength = float(CAM_array.shape[0])
    GPSIdx = arange(GPSlength)
    CAMIdx = arange(CAMlength)

    print GPSlength
    print CAMlength

    lengthScale = GPSlength/CAMlength
    CAMIdxScale = array(CAMIdx*lengthScale, dtype=int)
    scaledGPSlist = GPS_array[CAMIdxScale]
    
    return scaledGPSlist
    

GPS_array = loadtxt(sys.argv[1], delimiter =' ', dtype=float, skiprows=0)

dirlist = os.listdir(os.getcwd())
dirlist.sort()

CAM_array = array([i for i in dirlist if i[-4:].lower() == '.jpg'.lower()], dtype=str)

scaledGPSlist = array(scaleList(GPS_array, CAM_array), dtype=float)

#CAMlength = float(CAM_array.shape[0])

#outArray = empty((CAMlength,4))

header = ['# <label>',	'<x>',	'<y>','<z>']

stackedList = array(vstack((CAM_array, scaledGPSlist.transpose())).transpose(), dtype=object)

stackedList = array(vstack((header, stackedList)))

savetxt('rough_camera_XYZ_from_GPS.txt',stackedList, delimiter='\t', fmt ='%s')
